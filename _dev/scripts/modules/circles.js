var flag = true;

module.exports = {
    init: function(wScroll) {

        var skillsSection = $('.skills'),
            circles = $('.skills__circles'),
            skillsPos = skillsSection.offset().top,
            windowHeight = $(window).height() / 2;

        if (wScroll > skillsPos - windowHeight) {

            if (flag) {
                flag = false;

                var counter = 0,
                    timer;

                skillsSection.fadeTo(500, 1);

                function each() {
                    var $this = circles.eq(counter),
                        percents = 100 - parseInt($this.data('percents')),
                        circle = $this.find('.skills__circles-above'),
                        total = parseInt(circle.css('stroke-dasharray')),
                        reqWidth = (total / 100) * percents;

                    circle.css({
                        'stroke-dashoffset' : reqWidth
                    });

                    $this.fadeTo(200, 1);

                    if (typeof timer !== 'undefined') {
                        clearTimeout(timer);
                    }

                    timer = setTimeout(each, 200);

                    counter++;
                };

                each();
            }
        }
    }
}

var
    authLink =  $('.autherize-link'),
    authBlock = $('.auth__block'),
    activeClass = 'auth__block_login';

module.exports = {
    init: function () {
        
        // показать плашку
        authLink.on('click', function(e){
            e.preventDefault();

            var $this = $(this);

            $this.fadeOut();
            authBlock.addClass(activeClass);
        });
        
        // скрыть плашку
        $(document).on('click touchstart', function(e){
            var target = $(e.target),
                notPlank = !target.closest('.auth__block').length,
                notBtn = !target.hasClass('autherize-link');

            if (notPlank && notBtn) {
                authLink.fadeIn();
                authBlock.removeClass(activeClass);
            }
        });
    }
}
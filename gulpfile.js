var fs = require('fs');

/* --------- plugins --------- */

var	gulp        = require('gulp');
var	compass     = require('gulp-compass');
var	jade        = require('gulp-jade');
var	browserSync = require('browser-sync').create();
var	browserify  = require('gulp-browserify');
var	uglify      = require('gulp-uglify');
var	rename      = require("gulp-rename");
var	plumber     = require('gulp-plumber');
var	concat      = require('gulp-concat');
var	wiredep     = require('wiredep').stream;
var	useref      = require('gulp-useref');
var	gulpif      = require('gulp-if');
var shell       = require('gulp-shell');
var cssnano     = require('gulp-cssnano');

/* --------- paths --------- */

var DEV_PATH = '_dev';

var	paths = {
		jade : {
			location    : DEV_PATH + '/markups/**/*.jade',
			compiled    : DEV_PATH + '/markups/_pages/*.jade',
			destination : './pages'
		},

		scss : {
			location    : DEV_PATH + '/styles/**/*.scss',
			entryPoint  : DEV_PATH + '/styles/main.scss'
		},

		compass : {
			entryPoint  : DEV_PATH + '/styles/main.scss',
			configFile  : 'config.rb',
			cssFolder   : 'css',
			scssFolder  : DEV_PATH + '/styles',
			imgFolder   : 'img'
		},

		js : {
			location    : DEV_PATH + '/scripts/**/*.js',
			plugins     : DEV_PATH + '/scripts/_plugins/*.js',
			entyPoint   : DEV_PATH + '/scripts/main.js',
			destination : 'js'
		},

		browserSync : {
			baseDir : './',
			watchPaths : ['pages/*.html', 'css/*.css', 'js/*.js']
		},

		indexBuilder : {
			location : DEV_PATH + '/index_builder.js'
		}
	}

/* --------- jade --------- */

gulp.task('jade', function() {
	var assets = useref.assets();
	var YOUR_LOCALS = './content.json';

	gulp.src(paths.jade.compiled)
		.pipe(plumber())
		.pipe(jade({
			pretty: '\t',
			// locals: JSON.parse(fs.readFileSync(YOUR_LOCALS, 'utf8')),
			basedir : './'
		}))
		.pipe(assets)
		.pipe(gulpif('*.js', uglify()))
		.pipe(gulpif('*.css', cssnano()))
		.pipe(assets.restore())
		.pipe(useref())
		.pipe(gulp.dest(paths.jade.destination));
});

/* --------- scss-compass --------- */

gulp.task('compass', function() {
	gulp.src(paths.scss.entryPoint)
		.pipe(plumber())
		.pipe(compass({
			config_file: paths.compass.configFile,
			css: paths.compass.cssFolder,
			sass: paths.compass.scssFolder,
			image: paths.compass.imgFolder
		}));
});

/* --------- browser sync --------- */

gulp.task('sync', function() {
	browserSync.init({
		server: {
			baseDir: paths.browserSync.baseDir
		}
	});
});

/* --------- scripts --------- */

gulp.task('scripts', function() {
	return gulp.src(paths.js.entyPoint)
		.pipe(plumber())
		.pipe(browserify({
			insertGlobals : true,
			debug : true,
		}))
		.pipe(uglify())
		.pipe(rename('main.min.js'))
		.pipe(gulp.dest(paths.js.destination));
});

/* --------- index-page builder --------- */

gulp.task('exec', shell.task("node " + paths.indexBuilder.location));

/* --------- watch --------- */

gulp.task('watch', function(){
	gulp.watch(paths.jade.location, ['jade']);
	gulp.watch(paths.scss.location, ['compass']);
	gulp.watch(paths.js.location, ['scripts']);
	//gulp.watch(paths.js.plugins, ['plugins']);
	gulp.watch(paths.jade.destination, ['exec']);
	// gulp.watch(paths.browserSync.watchPaths).on('change', browserSync.reload);
});

/* --------- default --------- */

gulp.task('default', ['jade', 'compass', 'scripts', 'sync', 'exec', 'watch']);